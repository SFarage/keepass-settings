# KeePass Config

## Purpose
This is a repo for some KeePass config, info/settings and various comments perhaps.

## Use
Useful when using KP on multiple machines, keeping various installs 'in sync', small offices, etc.

KeePass documentation relating to [configuration](http://keepass.info/help/base/configuration.html).

Generally, download and copy `KeePass.config.xml` into your local KP directory

## Security thoughts
Coming...later.